package com.example;

import POGOProtos.Networking.Envelopes.RequestEnvelopeOuterClass;
import com.pokegoapi.api.PokemonGo;
import com.pokegoapi.api.device.DeviceInfo;
import com.pokegoapi.api.device.SensorInfo;
import com.pokegoapi.api.map.pokemon.CatchablePokemon;
import com.pokegoapi.auth.PtcCredentialProvider;
import com.pokegoapi.exceptions.AsyncPokemonGoException;
import com.pokegoapi.exceptions.LoginFailedException;
import com.pokegoapi.exceptions.RemoteServerException;
import java.util.List;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import rx.Observable;
import rx.Subscriber;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

/**
 * @author s.grochowiak
 */
public class PokeManager {

    public static final String USER_NAME = "";
    public static final String PASSWORD = "";

    public static final int TIMER_INTERVAL = 10000;
    public static final long RETRIES = 3;

    public static final double WATCHING_PLACE_LATITUDE = 51.105099d;
    public static final double WATCHING_PLACE_LONGITUDE = 17.021063d;

    private PokemonGo mGo;
    private CompositeSubscription mCompositeSubscription = new CompositeSubscription();
    private Action1 mOnPokemonResponseAction;

    private boolean mLastConnectionFailed;
    private boolean mIsBanned;

    public PokeManager(Action1<List<CatchablePokemon>> onPokemonResponseAction) {
        mOnPokemonResponseAction = onPokemonResponseAction;

        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BASIC);
        OkHttpClient http = new OkHttpClient.Builder().addInterceptor(httpLoggingInterceptor).build();

        RequestEnvelopeOuterClass.RequestEnvelope.AuthInfo auth = null;
        System.out.println("Login as " + USER_NAME);
        try {
            //or google
            //new PokemonGo(GoogleCredentialProvider(http,listner));
            //Subsiquently
            //new PokemonGo(GoogleCredentialProvider(http,refreshtoken));
            PtcCredentialProvider provider = new PtcCredentialProvider(http, USER_NAME, PASSWORD);
            mCompositeSubscription.add(Observable.just(new PokemonGo(provider, http))
                    .retry(RETRIES)
                    .subscribe(new Subscriber<PokemonGo>() {
                        @Override
                        public void onCompleted() {

                        }

                        @Override
                        public void onError(Throwable e) {
                            mLastConnectionFailed = true;
                            if (e instanceof LoginFailedException) {
                                if (e.getMessage() != null) {
                                    if (e.getMessage().contains("banned")) {
                                        mIsBanned = checkIfBanned(e);
                                    }
                                }
                            }
                        }

                        @Override
                        public void onNext(PokemonGo go) {
                            System.out.println("Pokemon API connected");
                            mGo = go;
                            mGo.setDeviceInfo(DeviceInfo.DEFAULT);

                            assignLocation();
                            assignSensorInfo();

                            mCompositeSubscription.add(Observable.interval(1000, TIMER_INTERVAL, TimeUnit.MILLISECONDS)
                                    .subscribeOn(Schedulers.io())
                                    .subscribe(new Action1<Long>() {
                                        @Override
                                        public void call(Long aLong) {
                                            System.out.println("Timer event");
                                            onTimerEvent();
                                        }
                                    }));
                        }
                    }));
        } catch (LoginFailedException | RemoteServerException | AsyncPokemonGoException e) {
            // failed to login, invalid credentials, auth issue or server issue.
            System.out.println("Failed to login or server issue: " + e.toString());
            mLastConnectionFailed = true;
            mIsBanned = checkIfBanned(e);
        }
    }

    public void onTimerEvent() {
        // set location
        // Fake location around
        assignSensorInfo();
        assignLocation();
        mCompositeSubscription.add(
                mGo.getMap().getCatchablePokemonAsync().subscribe(mOnPokemonResponseAction, new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {
                        throwable.printStackTrace();
                        mLastConnectionFailed = true;
                    }
                }));
    }

    private DeviceInfo createDeviceInfo() {
        DeviceInfo deviceInfo = new DeviceInfo();
        deviceInfo.setAndroidBoardName("");
        deviceInfo.setAndroidBootloader("");
        deviceInfo.setDeviceBrand("");
        deviceInfo.setDeviceId("");
        deviceInfo.setDeviceModel("");
        deviceInfo.setDeviceModelBoot("");
        deviceInfo.setDeviceModelIdentifier("");
        deviceInfo.setFirmwareBrand("");
        deviceInfo.setFirmwareFingerprint(
                "rk30sdk/rk30sdk/rk30sdk:4.4/JRO03H/eng.lfwang.20121026" + ".092031:eng/release-keys");
        deviceInfo.setFirmwareTags("");
        deviceInfo.setFirmwareType("");
        deviceInfo.setHardwareManufacturer("");
        deviceInfo.setHardwareModel("");
        return deviceInfo;
    }

    private void assignLocation() {
        Random random = new Random();
        double randValueLat = random.nextDouble() * 0.00001D;
        double randValueLong = random.nextDouble() * 0.00001D;
        double randValueAlt = random.nextDouble() * 10.0D;
        mGo.setLocation(WATCHING_PLACE_LATITUDE + randValueLat, WATCHING_PLACE_LONGITUDE + randValueLong,
                100 + randValueAlt);
    }

    private void assignSensorInfo() {
        Random random = new Random();
        SensorInfo sensorInfo = new SensorInfo();
        sensorInfo.setAngleNormalizedZ(random.nextDouble());
        sensorInfo.setAccelNormalizedY(random.nextDouble());
        sensorInfo.setAccelNormalizedX(random.nextDouble());
        mGo.setSensorInfo(sensorInfo);
    }

    public void close() {
        mCompositeSubscription.unsubscribe();
    }

    public boolean isLastConnectionFailed() {
        return mLastConnectionFailed;
    }

    public void setLastConnectionFailed(boolean lastConnectionFailed) {
        mLastConnectionFailed = lastConnectionFailed;
    }

    public boolean isBanned() {
        return mIsBanned;
    }

    private boolean checkIfBanned(Throwable e) {
        if (e instanceof LoginFailedException) {
            if (e.getMessage() != null) {
                if (e.getMessage().contains("banned")) {
                    return true;
                }
            }
        }
        return false;
    }
}
