package com.example;

import com.google.code.chatterbotapi.ChatterBot;
import com.google.code.chatterbotapi.ChatterBotFactory;
import com.google.code.chatterbotapi.ChatterBotSession;
import com.google.code.chatterbotapi.ChatterBotType;
import com.pokegoapi.api.map.pokemon.CatchablePokemon;
import com.pokegoapi.api.map.pokemon.encounter.EncounterResult;
import com.pokegoapi.exceptions.LoginFailedException;
import com.pokegoapi.exceptions.RemoteServerException;
import com.ullink.slack.simpleslackapi.events.SlackMessagePosted;
import com.ullink.slack.simpleslackapi.listeners.SlackMessagePostedListener;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

public class RunBot {

    public static boolean RUN_APP = true;

    public static final String GOOGLE_MAP_URL = "http://maps.googleapis.com/maps/api/staticmap?center="
            + PokeManager.WATCHING_PLACE_LATITUDE
            + ","
            + PokeManager.WATCHING_PLACE_LONGITUDE
            + "&size=300x300&zoom=17&markers=color:blue%7Clabel:S%7C";

    public static SlackManager mSlackManager;
    public static PokeManager mPokeManager;
    public static ChatterBotSession mCleverBotSession;

    private static boolean mTimeToHome;

    public static Set<Long> mEncounterIdSet = new HashSet<>();

    private static List<CatchablePokemon> mLastCatchablePokemonList = new ArrayList<>();

    public static void main(String[] args) throws IOException {

        Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
            public void run() {
                close();
                RUN_APP = false;
                System.out.println("Bot stopped");
            }
        }));
        start();
    }

    private static void start() {
        initSlack();
        if (mSlackManager.isConnected()) {
            System.out.println("Slack connected");
            mSlackManager.addMessagePostedListener(onSlackMessagePostedListener());
            initCleverBot();
            initPoke();
            Scanner scan = new Scanner(System.in);
            while (RUN_APP) {
                String input = scan.nextLine();
                if (input != null) {
                    if (input.equals("exit")) {
                        RUN_APP = false;
                        System.out.println("Exited manually");
                        return;
                    }
                    String[] splitInput = input.split(" ");
                    if (splitInput[0].equals("say")) {
                        if (splitInput.length > 1) {
                            mSlackManager.sendResponseToChannel(input.replaceFirst("say ", ""));
                        }
                    }
                }
            }
        } else {
            System.out.println("Slack connection problem");
        }
    }

    private static void close() {
        mSlackManager.close();
        if (mPokeManager != null) {
            mPokeManager.close();
        }
    }

    private static void initSlack() {
        mSlackManager = new SlackManager();
        mSlackManager.start();
    }

    private static void initPoke() {
        mPokeManager = new PokeManager(catchablePokemons -> {
            System.out.println("Pokemon in area:" + catchablePokemons.size());
            mPokeManager.setLastConnectionFailed(false);
            mLastCatchablePokemonList = catchablePokemons;

            StringBuilder stringBuilder = new StringBuilder();
            for (CatchablePokemon cp : catchablePokemons) {
                // You need to Encounter first.
                EncounterResult encResult = null;
                try {
                    encResult = cp.encounterPokemon();
                    // if encounter was succesful, catch
                    if (encResult.wasSuccessful()) {
                        System.out.println("Encounted:" + cp.getPokemonId());
                        if (!mEncounterIdSet.contains(cp.getEncounterId())) {
                            stringBuilder.append(cp.getPokemonId()).append(" ");
                            mEncounterIdSet.add(cp.getEncounterId());
                        }

                        /*cp.catchPokemonAsync(Pokeball.POKEBALL).subscribe(catchResult -> {
                            if (catchResult.isFailed()) {

                            }
                        });*/
                        //Log.e("Main", "Attempt to catch:" + cp.getPokemonId() + " " + result.getStatus());
                    }
                } catch (LoginFailedException | RemoteServerException e) {
                    e.printStackTrace();
                }
            }

            if (catchablePokemons.size() > 0 && stringBuilder.length() > 0) {
                stringBuilder.append("appeared");
                mSlackManager.sendMessageToAChannel(stringBuilder.toString());
            }
        });
    }

    private static void initCleverBot() {
        ChatterBotFactory factory = new ChatterBotFactory();

        ChatterBot cleverBot = null;
        try {
            cleverBot = factory.create(ChatterBotType.CLEVERBOT);
            mCleverBotSession = cleverBot.createSession();
            System.out.println("Clever bot connected");
            mCleverBotSession.think("Do you like pokemon?");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static SlackMessagePostedListener onSlackMessagePostedListener() {
        return (event, session) -> {
            System.out.println("Event received");
            if (mSlackManager.getSession().sessionPersona().getId().equals(event.getSender().getId())) {
                return;
            }
            String messageContent = event.getMessageContent();
            String[] splitMessageContent = messageContent.split(" ");
            if (splitMessageContent.length == 0) {
                return;
            }

            String botUser = "<@" + mSlackManager.getSession().sessionPersona().getId() + ">";
            if (splitMessageContent[0].equals(botUser)) {
                System.out.println("Message to me: " + messageContent);
                if (splitMessageContent.length == 1) {
                    sendMessageFromCleverBot("Where pokemons are?");
                    return;
                }
                if (splitMessageContent.length == 2) {
                    switch (splitMessageContent[1]) {
                        case "help":
                            // TODO change command strings to enum
                            StringBuilder helpBuilder = new StringBuilder("Available commands:\n").append("help\n")
                                    .append("status\n")
                                    .append("pokestatus\n")
                                    .append("pokeshow\n")
                                    .append("pokeexpire\n")
                                    .append("pokeloc\n");
                            mSlackManager.sendResponseToChannel(helpBuilder.toString());
                            return;
                        case "status":
                            mSlackManager.sendResponseToChannel("I'm alive!");
                            if (mPokeManager.isBanned()) {
                                mSlackManager.sendResponseToChannel("I'm fukin banned bro!");
                            }
                            return;
                        case "pokestatus":
                            if (mPokeManager.isLastConnectionFailed()) {
                                mSlackManager.sendResponseToChannel("Last connection to pokemon API failed");
                            } else {
                                mSlackManager.sendResponseToChannel("Connection to pokemon API is stable");
                            }
                            return;
                        case "pokeexpire":
                            commandPokeexpire();
                            return;
                        case "pokeshow":
                            commandPokeShow();
                            return;
                        case "pokeloc":
                            commandPokeLoc();
                            return;
                        case "restore":
                            commandRestore(event);
                            return;
                    }
                }
                String messageForCleverBot = messageContent.replaceFirst(botUser + " ", "");
                sendMessageFromCleverBot(messageForCleverBot + " Pokemon");
            }
        };
    }

    public static void sendMessageFromCleverBot(String messageForCleverBot) {
        String cleverBotResponse = null;
        try {
            cleverBotResponse = mCleverBotSession.think(messageForCleverBot);
            mSlackManager.sendResponseToChannel(cleverBotResponse);
        } catch (Exception e) {
            e.printStackTrace();
            initCleverBot();
        }
    }

    public static void commandPokeexpire() {
        if (mLastCatchablePokemonList != null && !mLastCatchablePokemonList.isEmpty()) {
            StringBuilder stringBuilder = new StringBuilder("Catchable pokemons:\n");
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss");
            for (CatchablePokemon catchablePokemon : mLastCatchablePokemonList) {
                EncounterResult encResult = null;
                try {
                    encResult = catchablePokemon.encounterPokemon();
                    // if encounter was succesful, catch
                    if (encResult.wasSuccessful()) {
                        stringBuilder.append(catchablePokemon.getPokemonId())
                                .append(" expires at ")
                                .append(simpleDateFormat.format(new Date(catchablePokemon.getExpirationTimestampMs())))
                                .append("\n");
                    }
                } catch (LoginFailedException | RemoteServerException e) {
                    e.printStackTrace();
                }
            }
            mSlackManager.sendResponseToChannel(stringBuilder.toString());
        } else {
            mSlackManager.sendResponseToChannel("No available pokemons");
        }
    }

    public static void commandRestore(SlackMessagePosted event) {
        if (event.getSender()
                .getId()
                .equals(mSlackManager.getSession().findUserByUserName(SlackManager.BOT_MASTER).getId())) {
            mSlackManager.sendResponseToChannel("Restarting");
            close();
            try {
                Thread.sleep(3000);
                start();
                mSlackManager.sendResponseToChannel("Connection has been restored");
            } catch (InterruptedException e) {
                e.printStackTrace();
                mSlackManager.sendResponseToChannel("Connection fucked up");
            }
        }
    }

    public static void commandPokeShow() {
        if (mLastCatchablePokemonList != null && !mLastCatchablePokemonList.isEmpty()) {
            StringBuilder stringBuilder = new StringBuilder("Catchable pokemons:\n");
            for (CatchablePokemon catchablePokemon : mLastCatchablePokemonList) {
                EncounterResult encResult = null;
                try {
                    encResult = catchablePokemon.encounterPokemon();
                    // if encounter was succesful, catch
                    if (encResult.wasSuccessful()) {
                        stringBuilder.append("http://www.pokestadium.com/sprites/xy/")
                                .append(catchablePokemon.getPokemonId().toString().toLowerCase())
                                .append(".gif")
                                .append("\n");
                    }
                } catch (LoginFailedException | RemoteServerException e) {
                    e.printStackTrace();
                }
            }
            mSlackManager.sendResponseToChannel(stringBuilder.toString());
        } else {
            mSlackManager.sendResponseToChannel("No available pokemons");
        }
    }

    public static void commandPokeLoc() {
        if (mLastCatchablePokemonList != null && !mLastCatchablePokemonList.isEmpty()) {
            StringBuilder stringBuilder = new StringBuilder("Catchable pokemons:\n");
            for (CatchablePokemon catchablePokemon : mLastCatchablePokemonList) {
                EncounterResult encResult = null;
                try {
                    encResult = catchablePokemon.encounterPokemon();
                    // if encounter was succesful, catch
                    if (encResult.wasSuccessful()) {
                        stringBuilder.append(catchablePokemon.getPokemonId())
                                .append(" location is \n").append(GOOGLE_MAP_URL)
                                .append(catchablePokemon.getLatitude())
                                .append(",")
                                .append(catchablePokemon.getLongitude())
                                .append("\n");
                        /*
                        stringBuilder.append(catchablePokemon.getPokemonId())
                                .append(" location is ")
                                .append(catchablePokemon.getLatitude())
                                .append(" ")
                                .append(catchablePokemon.getLongitude())
                                .append("\n");
                                */
                    }
                } catch (LoginFailedException | RemoteServerException e) {
                    e.printStackTrace();
                }
            }
            mSlackManager.sendResponseToChannel(stringBuilder.toString());
        } else {
            mSlackManager.sendResponseToChannel("No available pokemons");
        }
    }
}
