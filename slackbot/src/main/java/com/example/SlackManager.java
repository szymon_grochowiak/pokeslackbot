package com.example;

import com.ullink.slack.simpleslackapi.SlackChannel;
import com.ullink.slack.simpleslackapi.SlackSession;
import com.ullink.slack.simpleslackapi.impl.SlackSessionFactory;
import com.ullink.slack.simpleslackapi.listeners.SlackMessagePostedListener;
import java.io.IOException;

/**
 * @author s.grochowiak
 */
public class SlackManager {

    public static final String API_TOKEN = "";

    public static final String BOT_NAME = "";
    public static final String POKEMON_CHANNEL_NAME = "";
    public static final String BOT_MASTER = "";

    SlackSession mSession;

    public SlackManager() {
        mSession = SlackSessionFactory.createWebSocketSlackSession(API_TOKEN);
    }

    public void sendMessageToAChannel(String message) {
        //get a channel
        SlackChannel channel = mSession.findChannelByName(POKEMON_CHANNEL_NAME);
        mSession.sendMessage(channel, message + "\n@channel");
    }

    public void sendResponseToChannel(String message) {
        //get a channel
        SlackChannel channel = mSession.findChannelByName(POKEMON_CHANNEL_NAME);
        mSession.sendMessage(channel, message);
    }

    public void addMessagePostedListener(SlackMessagePostedListener slackMessagePostedListener) {
        if (mSession.isConnected()) {
            mSession.addMessagePostedListener(slackMessagePostedListener);
        }
    }

    public SlackSession getSession() {
        return mSession;
    }

    public boolean start() {
        try {
            mSession.connect();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean isConnected() {
        return mSession.isConnected();
    }

    public void close() {
        if (mSession.isConnected()) {
            try {
                mSession.disconnect();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
